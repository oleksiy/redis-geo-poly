import numpy as np
import cProfile
import redis

low_range = [-15, -177]
high_range = [72, 146]

red = redis.StrictRedis()
pipe = red.pipeline()
with open('./geo.lua', 'r') as file:
    script = red.register_script(file.read())
    
print(cProfile.run('''
for i in range(1000):
    rand_coord = np.random.uniform(low_range, high_range)
    script(args=['FINDPOLYSI', rand_coord[0], rand_coord[1]], keys=['poly_zips'], client=pipe)
    pipe.execute()
'''
))