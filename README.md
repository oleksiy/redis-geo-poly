# Redis GEO Polygon search

Based on https://github.com/RedisLabs/geo.lua

## Usage

### Load data to Redis

You can use `python` script here in `create_polys.py` to load any shape data
For ex., https://www.census.gov/geographies/mapping-files/2018/geo/carto-boundary-file.html, or any other polygons, like stored datasets in `geopandas`, etc.

### Load script

To load script specifically in Redis CLI you can do

```sh
$ redis-cli SCRIPT LOAD "$(cat geo.lua)"
"XX"
$ redis-cli EVALSHA XX 1 poly_zips FINDPOLYSI -81.282078 28.564349
```

The example above shows that we run function `FINDPOLYSI` on a set of polygons `poly_zips` to find all polygons which cover point with coordinates `(-81.282078 28.564349)`

## Use case

Generally speaking for the single specific 2D point we want to find all polygons that cover it.

We assume that point is single, but we have millions of polygons.

ElasticSearch calls this functionality "percolate" (aka reverse search) and really disappoints in performance.

### Example

You want to know when restaurants will reopen in some area, or city, or county. Any of this (area, city, county) is a polygon and the restaurant is a location point.

There are millions of people like you who want to get notification about reopening.  And single person notification is defined by area (aka polygon).

As soon as the restaurant has open information we need to find all people whose area covers this restaurant and notify.

You may imagine many more scenarios where you may need to find multitudes of polygons covering a single point.

## Implementation

### `FINDPOLYS`

This simple implementation just iterates all polygons and selects which covers the point. Performance improvement is minimal and only filters out by boundary box which is relatively fast checked.

Not much upgrade from base `geo.lua` and intended mostly to get familiar with data types and command APIs.

Also, it sucks in performance almost as much as ElasticSearch.

### `FINDPOLYSI`

Here we are making things more interesting:

1. We add a new type of indexing to the Polygon object:

  Imagine a matrix of all coordinates: from -180 to 180 and from -90 to 90.

  You'll have about 65k of points and we build index around this.

  For each `GEOMETRYADD` call we remember which points are covered by the boundary box of our polygon (which is really simple and fast operation).

2. When we search for a point we take 4 points which define square around  our point and lookup for a union of all polygons hovering this square (Redis `SET` operations)

When you do this really fast pre-filter you can do precise check for your specific point which will have to iterate a much smaller set of polygons.

This indexing approach is significantly better in terms of speed.


### Future improvements

Of course, index costs memory. But further speed improvements may be reached by increasing index precision (from 1 degree to 0.1 or 0.01) in the cost of even more memory.

Other indexing improvements for really lightning pre-filter may be based on use-cases and having/saving more information about areas, not only coordinates of polygonal points.

What's more, you can eliminate second precision filtering if your pre-filter is good enough and its results will satisfy the use case.


## Performance

Performance was measured with `perf.py` script, by generating a random point and calling search function on it 100 times. Obtained results:  
* 100 `FINPOLYS` calls took 1126.026 seconds, which is ~11 seconds per call;
* 100 `FINDPOLYSI` calls took 3.741 seconds, which is ~37 milliseconds per call;  
  
It can be seen that adding index gave a significant improvement in performance.

# License

MIT

# Authors
[Oleksiy Babich](mailto:oleksiy@oleksiy.od.ua) and [Ihor Markevych](mailto:ih.markevych@gmail.com)


